<?php

$host = '127.0.0.1';
$port = '3308';
$username = "root";
$password = "";
$dbname = "mydb";
try {
    // Create connection

    $db= new PDO("mysql:host=$host;dbname=$dbname;port=$port", $username, $password);
    // set the PDO error mode to exception
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $db->prepare("SELECT * FROM `lq`");
    $results = $stmt->execute();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr><td>".$row['Label']."</td>";
        echo "<td>".$row['Type']."</td>";
        echo "<td>".$row['Volume']." ml</td></tr>"; 
    }


    $db = null;
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>